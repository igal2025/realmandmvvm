//
//  PangoHomeAssignmentTests.swift
//  PangoHomeAssignmentTests
//
//  Created by Igal Leibovich on 8/20/18.
//  Copyright © 2018 Igal Leibovich. All rights reserved.
//

import XCTest

@testable import PangoHomeAssignment

class PangoHomeAssignmentTests: XCTestCase {
    
    var currentExpectaion: XCTestExpectation?
    let queue = DispatchQueue(label: "background")
    
    override func setUp() {
        super.setUp()
        
        self.queue.async {
            RealmManager.sharedInstance.deleteAllFromDatabase()
            let item = DataItem()
            item.count = 8
            RealmManager.sharedInstance.addData(object: item)
        }        
    }
    
    func testPlus()
    {
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let vm = CountingViewModel(dataSource: CountingDataSource())
                vm.populateOperation(operation: .plus)
                XCTAssertEqual(vm.count, 9)
            }
        }
    }
    
    func testMinus()
    {
        self.queue.async {
            let vm = CountingViewModel(dataSource: CountingDataSource())
            vm.populateOperation(operation: .minus)
            XCTAssertEqual(vm.count, 7)
        }
    }
    
    func testReset()
    {
        self.queue.async {
            let vm = CountingViewModel(dataSource: CountingDataSource())
            vm.populateOperation(operation: .reset)
            XCTAssertEqual(vm.count, 0)
        }
    }
    
    func testGetData()
    {
        self.queue.async {
            let vm = CountingViewModel(dataSource: CountingDataSource())
            vm.bootstrap()
            XCTAssertEqual(vm.count, 8)
        }
    }
    
    override func tearDown() {
        
        //RealmManager.sharedInstance.deleteAllFromDatabase()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
