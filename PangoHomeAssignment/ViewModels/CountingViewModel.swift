//
//  ViewModel.swift
//  PangoHomeAssignment
//
//  Created by Igal Leibovich on 8/20/18.
//  Copyright © 2018 Igal Leibovich. All rights reserved.
//

import Foundation
import RealmSwift

/// - willLoadData - responsible to report about process before loading data
/// - didRecieveDataUpdate - responsible to report about the finish loading data with the count result paramater.
protocol ViewModelDelegate: class {
    
    func willLoadData()
    func didRecieveDataUpdate(count: Int)
}

/// - bootstrap - initializetion
/// - Hold the ViewModelDelegate
protocol ViewModelType {
    func bootstrap()
    var delegate: ViewModelDelegate? { get set }
}

/// - count - Holding the count value
/// - populateOperation - Process operations (Add/Sub/Reset) against Realm Database.
protocol CountingViewModelType: ViewModelType {
    
    var count: Int { get }
    func populateOperation(operation: OperationType)
}

class CountingViewModel: CountingViewModelType {
    
    var count: Int
    var delegate: ViewModelDelegate?    
    let dataSource: CountingDataSourceType
    
    init(dataSource: CountingDataSourceType) {
        self.dataSource = dataSource
        self.count = 0
    }
    
    func bootstrap() {
        
        self.refreshData()
    }
    
    private func refreshData() {
        
        delegate?.willLoadData()
        dataSource.fetchData(completion: { [weak self] result in
            
            DispatchQueue(label: "background").async {
                guard let data = self else { return }
                
                switch result {
                    
                    case.failure(_):
                        data.count = 0
                    case.success(let countResult):
                        data.count = countResult
                        data.delegate?.didRecieveDataUpdate(count: countResult)
                }
            }
        })
    }
    
    func populateOperation(operation: OperationType) {
        
        self.dataSource.populateOperation(operation: operation)
        self.refreshData()
    }
}
