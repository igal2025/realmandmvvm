//
//  CountingUsersDataSource.swift
//  PangoHomeAssignment
//
//  Created by Igal Leibovich on 8/22/18.
//  Copyright © 2018 Igal Leibovich. All rights reserved.
//
// https://realm.io/docs/swift/latest/

import Foundation
import RealmSwift

/// - fetchData - get latest count vlue from DataItem object.
/// - populateOperation - process the user operation against RealmManager.
protocol CountingDataSourceType {
    
    func fetchData(completion: @escaping (_ result: Result<Int>) -> Void)
    func populateOperation(operation: OperationType)
}

/// CountingDataSource is a data layer that responsible
/// for all operations against RealmManager
class CountingDataSource: CountingDataSourceType {
    
    /// - Validate if database exist - else create new database with dataItem object.
    /// - Get the dataItem object and extract the count value.
    /// - Handle completion object with the result.
    func fetchData(completion: @escaping (_ result: Result<Int>) -> Void) {
        
        if RealmManager.sharedInstance.databseIsEmpty() {
            RealmManager.sharedInstance.initDatabase()
        }
        else {
            if let object = RealmManager.sharedInstance.getDataFromDB()?.first {
                let result: Result<Int> = Result.success(object.count)
                completion(result as Result<Int>)
            }
        }
    }
    
    /// Get the DataItem object from Realm database.
    /// Add/subtract/reset according "OperationType".
    /// Save DataItem to Realm database with "Update" flag = true.
    func populateOperation(operation: OperationType) {
        
        if let object = RealmManager.sharedInstance.getDataFromDB() {
            
            if let oldValue = object.first {
                
                let item = DataItem()
                
                switch operation {
                case .plus:
                    item.count = oldValue.count + 1
                case .minus:
                    item.count = oldValue.count - 1
                case .reset:
                    item.count = 0
                case .none:
                    break
                }
                
                RealmManager.sharedInstance.addData(object: item)
            }
        }
    }
}
