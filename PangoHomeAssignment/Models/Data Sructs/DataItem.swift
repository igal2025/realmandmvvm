//
//  DataItem.swift
//  PangoHomeAssignment
//
//  Created by Igal Leibovich on 8/21/18.
//  Copyright © 2018 Igal Leibovich. All rights reserved.
//

import Foundation
import RealmSwift

class DataItem: Object
{
    @objc dynamic var id = 1
    @objc dynamic var count = 0
    
    override static func primaryKey() -> String? {
        return "id"
    } 
}
