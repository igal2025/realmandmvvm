//
//  RealmManager.swift
//  PangoHomeAssignment
//
//  Created by Igal Leibovich on 8/24/18.
//  Copyright © 2018 Igal Leibovich. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    
    private var database: Realm
    
    /// Singlton of Realm databse object.
    static let sharedInstance = RealmManager()
    
    private init() {
        database = try! Realm()
    }
    
    /// Initialize the first creation when there is no databse exist.
    /// Create Realm DB, insert the DataItem object with count = 0
    func initDatabase(initValue: Int = 0)
    {
        let data = DataItem()
        data.count = initValue
        
        try! database.write {
            
            database.add(data)
        }
    }
    
    func databseIsEmpty() -> Bool {
        
        return database.isEmpty
    }
    
    /// Query Realm for DataItem object
    func getDataFromDB() -> Results<DataItem>? {
        
        let results: Results<DataItem> = database.objects(DataItem.self)
        return results
    }
    /// Save/Update DataItem object into Realm database
    func addData(object: DataItem) {
        
        try! database.write {
            database.add(object, update: true)
        }
    }
    
    /// Delete all objects from Realm database
    func deleteAllFromDatabase() {
        
        try! database.write {
            
            database.deleteAll()
        }
    }
    
    /// Delete single object from Realm database
    func deleteFromDb(object: DataItem) {
        
        try! database.write {
            
            database.delete(object)
        }
    }
}
