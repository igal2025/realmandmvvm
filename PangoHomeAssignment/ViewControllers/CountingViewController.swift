//
//  ViewController.swift
//  PangoHomeAssignment
//
//  Created by Igal Leibovich on 8/20/18.
//  Copyright © 2018 Igal Leibovich. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift

class CountingViewController: UIViewController {

    //MARK: - Properties
    
    private var lblCounting: UILabel!
    
    private var model: CountingViewModelType!
    
    static func create(model: CountingViewModelType) -> CountingViewController {
        let vc = CountingViewController()
        vc.model = model
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        model.delegate = self
        model.bootstrap()
        
        self.setViewController()
    }
    
    //MARK: - Buttons event handlers
    
    @objc func PlusButtonPressed(_ sender: AnyObject) {
        
        model.populateOperation(operation: .plus)
    }
    
    @objc func MinusButtonPressed(_ sender: AnyObject) {
        
        model.populateOperation(operation: .minus)
    }
    
    @objc func ResetButtonPressed(_ sender: AnyObject) {
        
        model.populateOperation(operation: .reset)
    }
    
    func setViewController()
    {
        self.view.backgroundColor = UIColor.white
        /// Create main StackView
        let stackViewMain = UIStackView()
        stackViewMain.widthAnchor.constraint(equalToConstant: 200).isActive = true
        stackViewMain.axis = .vertical
        stackViewMain.distribution = .fill
        stackViewMain.alignment = .fill
        stackViewMain.spacing = 0
        stackViewMain.translatesAutoresizingMaskIntoConstraints = false
        
        /// Create secondary StackView
        let stackViewSecondary = UIStackView()
        stackViewSecondary.axis = .horizontal
        stackViewSecondary.distribution = .fillEqually
        stackViewSecondary.alignment = .fill
        stackViewSecondary.spacing = 0
        stackViewSecondary.translatesAutoresizingMaskIntoConstraints = false
        
        /// Initialize counting label
        self.lblCounting = UILabel()
        self.lblCounting.backgroundColor = UIColor(red: 0.9765, green: 0.7412, blue: 0.9569, alpha: 1.0) /* #f9bdf4 */
        self.lblCounting.text  = "0"
        self.lblCounting.textAlignment = .center
        
        /// Create & initialize plus button
        let btnPlus = UIButton()
        btnPlus.setTitle("◀️", for: .normal)
        btnPlus.backgroundColor = UIColor(red: 0.2157, green: 0.949, blue: 0.9255, alpha: 1.0) /* #37f2ec */
        btnPlus.addTarget(self, action: #selector(PlusButtonPressed), for: .touchUpInside)
        
        /// Create & initialize minus button
        let btnMinus = UIButton()
        btnMinus.setTitle("▶️", for: .normal)
        btnMinus.backgroundColor = UIColor(red: 0.7294, green: 0.8588, blue: 0.7608, alpha: 1.0) /* #badbc2 */
        btnMinus.addTarget(self, action: #selector(MinusButtonPressed), for: .touchUpInside)
        
        /// Create & initialize reset button
        let btnReset = UIButton()
        btnReset.setTitle("⏹", for: .normal)
        btnReset.backgroundColor = UIColor(red: 0.7333, green: 0.949, blue: 0.9686, alpha: 1.0) /* #bbf2f7 */
        btnReset.addTarget(self, action: #selector(ResetButtonPressed), for: .touchUpInside)
        stackViewMain.addSubview(btnReset)
        
        /// StackViews: ArrangedSubview
        stackViewMain.addArrangedSubview(lblCounting)
        stackViewSecondary.addArrangedSubview(btnPlus)
        stackViewSecondary.addArrangedSubview(btnMinus)
        stackViewMain.addArrangedSubview(stackViewSecondary)
        stackViewMain.addArrangedSubview(btnReset)
        view.addSubview(stackViewMain)
        
        // stackView constraints
        stackViewMain.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        stackViewMain.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    deinit {
        
    }
}

extension CountingViewController: ViewModelDelegate {
  
    func willLoadData() {
    }
    
    func didRecieveDataUpdate(count: Int) {
        
        print("didRecieveDataUpdate with count = \(count)")
        DispatchQueue.main.async {
            self.lblCounting.text = "\(count)"
        }
    }
}
