//
//  AppDelegate.swift
//  PangoHomeAssignment
//
//  Created by Igal Leibovich on 8/20/18.
//  Copyright © 2018 Igal Leibovich. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        window.rootViewController = UINavigationController(rootViewController: CountingViewController.create(model: CountingViewModel(dataSource: CountingDataSource())))
        window.makeKeyAndVisible()
        self.window = window
        return true
    }
}

