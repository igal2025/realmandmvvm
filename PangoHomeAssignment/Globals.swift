//
//  Globals.swift
//  PangoHomeAssignment
//
//  Created by Igal Leibovich on 8/26/18.
//  Copyright © 2018 Igal Leibovich. All rights reserved.
//

import Foundation

internal enum OperationType {
    
    case none
    case plus
    case minus
    case reset
}


enum Result<T> {
    
    case success(T)
    case failure(Error)
}

internal var operationType: OperationType = .none
